Version 4.7.1

- Core

    * Minimal required GLib version is 2.8 (#1980)
    * Reorganization of source tree structure (#1866, #2037)
    * States of all 'Find File' dialog checkboxes are saved in user configuration file (#1874, #1965)
    * New file type bindings:
          o viewing .lyx with lyxcat, opening with lyx (#1693)
    * Added shortcut (Meta-,) to toggle panels split (#1991)
    * Capability to remove history items. !WListbox widget was fully reimplemented (#1445)
    * Autodetect codepages of edited/viewed files with enca program (#1838)
    * Custom/locale-based date format (#1767)
    * New quick search behavior, allow wildcard characters - '*' and '?'(#2022)
    * Panels: new sort type 'by version' (#1994)
    * Added 'menuinactive' skin item to draw inactive visible main menu (#1999)
    * Added ability to show progressbars (when copy files) from right to left (#1443)
    * Added indication of total BPS and ETA for file operations; fully rewrited FileOperations dialog (#1443)

- VFS

    * Small reorganizing (#1931)
    * Easy plugable EXTFS VFS (#1909)

- Editor

    * Some simple optimization of syntax highlighting engine (#1843)
    * Show right margin using 'editor_show_right_margin' option. Keybind EditToggleShowMargin in [editor] section is used to toggle the state (#1514)
    * New editor action 'Mark all', new keybind EditMarkAll (#1945)
    * Changed default for 'Cursor beyond EOL', now it switched off by default (#1946)
    * Changed default color pair for 'editbold' (search result) to be more sensible (#1559)

- Viewer

    * Viewer is now very fast (#1585)
    * Added new confirm box for repeat search from begining of file (#1917)

- Misc

    * Lots of code cleanup (#1780, #1915, #1990)
    * Removed obsolete checks in configuration scripts (#262, #1932)
    * Documentation updates
    * Translation updates

- Fixes

    * MC won't compile on Solaris due to undefined constant NAME_MAX (#1943)
    * MC won't compile on AIX (#1957)
    * Missing includes (#1930, #2017)
    * Missing printf format (#1960)
    * Memory and file descriptors leaks (#1953, #2028, #2053, #2058)
    * Small error in versioning (#1905)
    * Incorrect start up with some special paths (#1992)
    * Segfault in input line history (#1936)
    * MC crashes on exit when using C locale (#1942)
    * MC crashes at exit due to race conditions of destroying subshell and file manager (#2008)
    * Assertion failed for g_array_free (#1954)
    * Broken transparency if MC is built with NCurses (#1657)
    * Selections not visible on monochrome terminals (#1972)
    * Colors of visible inactive menu (#1702)
    * Incorrect input line length in 'Edit symlink' dialog window (#1955)
    * Unquoted shell variables in user menu (#1967)
    * Ctrl-\ key closes the NCurses-based MC (#1926)
    * verbose option is always on after MC start (#1940)
    * Show total progressbar (and related info) when copying just one subdirdir with lot of files (#1443)
    * Incorrecy movement in last line in editor (moving cursor to the empty last line) (#1956)
    * Incorrect editor lock files check (#1935)
    * Segfault at try of edit (F4) archives with utf-8 names (x86_64 specified) (#1982)
    * Editor's search parameters are not retained across editing session (#1572)
    * EditColumnMark can't go up through newline (#1998)
    * 'Confirm replace' dialog is not converted from UTF-8 to user defined charset (#1986)
    * Missed \s symbol in Syntax file (#2010)
    * Viewer in QuickView mode corrupts screen (#1918, #1944)
    * ViewContinueSearch segfault on empty search (#1996)
    * MC crashes if file name in archves contains '@' symbol (#1605)
    * deba and debd VFS: items from DEBIAN directory are not accessible (#1920)
    * Samba is not built with tcc (#1933)
    * Incorrect conditional compilation of mcserver (#1925)
    * Potencial security risk in mcserv (#1902)
    * The lslR VFS doesn't work with ls-lR files created in en_US.UTF-8 locale and with files and directories started with whitespaces (#1921)
    * Contents of RAR archives with filenames that contain / \d\d:\d\d / are not listed correctly (#2029)
    * FTPFS: strcpy() is used for overlaping strings (#2018)

Version 4.7.0.1

- Fixes

    * Fixed double free of memory in editor;
    * Fixed build with --disable-vfs option;
    * Fixed compile warnings about unused variables;
    * Fixed Fedora/RH RPM versioning scheme.


Version 4.7.0

- Core

    * Removed charset confirmation on startup and added the automatic recognize
      of system charset (#1613)
    * Some startup speed up (#1781)
    * Save flags of select/unselet file group (#1776)
    * Don't set TOSTOP attribute on slave terminal (#1637)
    * Keybindings for button bars (F1-F10 keys) (#212)
    * Keybindings for Tree widget
    * Enlarged sizes of Directory Tree dialog window
    * Added missed esc sequencies for qansi terminals (#1803)
    * New file type bindings:
      + adeed mkv, 3gp video types
      + use exif(1) output when viewing jpeg files
      + opening .chm with kchmviewer with fallback to xchm
      + viewing .mo files with msgunfmt

- VFS

    * FISH: add the user name to the ssh command line only if it's not the same as
      the local logged-in user (#1867)
    * FTP: support active mode (#1796)
    * FTP: improved symlink handling
    * FTP: some FTP-servers don't have chmon support. Ignore FTP errors related
      to chmod operations (#1708)
    * EXTFS: added S3 backend by Jakob Kemi (#272)
    * SFS: use single script for RPM and SRPM packets handling (#1590)

- Editor

    * New syntax files: Cabal
    * Updated syntax files: Python, PHP
    * Syntax highlighting: added '.hh' and '.hpp' file extensions as C++ sources

- Misc

    * Lots of code cleanup and optimization
    * Workaround for SunStudio compiler bug #6888373 (#1749)
    * Removed obsolete checks in configuration scripts
    * Allow save empty value in history (#1814)
    * Use ctrl-g instead of ctrl-c for SIGINT signal (#1876).
    * RPM spec: rewritten and simplified static build procedure for legacy distros (#1848)
    * Translation updated
    * Documentation updates
    * Added skin: darker color style (#1659)

- Fixes

    * Memory and file descriptors leaks
    * Crash on start if ~/.mc/panels.ini contains incorrect value of 'sort_order' key (#1790)
    * MC aborts when ctrl-c is pressed (#1793)
    * Build w/o internal editor (#1826)
    * Compilation warings of --disable-nls and --disable-charset options (#1831)
    * Incorrect handling of '--with-search-engine' configure option (#1396)
    * Segmentation fault when search in different encodings (#1858)
    * C-w does not delete whole command line (#407)
    * Wrong shortcuts in menu items (#1836)
    * Panels state saves and restores despite disabled 'Auto save setup' (#1663)
    * Case insensitive file sorting in UTF-8 locales (#1536)
    * Incorrect handling of 0xFF char that is valid char in CP1251 locale (#1883)
    * Segfault in TreeView loading (#1794)
    * Incorect stat info of ".." directory in file panel (#1757)
    * Incorect stat info of ".." directory in info panel (#1757)
    * Setting "Use passive mode over proxy" is not remembered (#1746)
    * CK_Edit_Save_Mode command handling in editor (#1810)
    * Incorrect calculation of some utf-8 characters width (#1768)
    * Handling CJK chars in editor (#1628)
    * Incorrect cursor positioning in editor (#1809, #1884)
    * Vertical block handling in editor (#1839)
    * Incorrect text scrolling in editor (#1877)
    * Incorrect mouse movement in editor when text scrolled to the right (#1792)
    * Newlines are lost when pasting multiline text in editor (#1710)
    * Mismatched hotkeys EditBeginRecordMacro, EditEndRecordMacro in editor
    * �ismatched hotkeys EditBeginningOfText, EditEndOfText, EditBeginPage, EditEndPage (#1724)
    * Some syntax files are not installable (#1779)
    * Date & time insertion in editor (#1759)
    * "Matches not found" message is not shown in viewer
    * Hangup after search in archive in viewer (#1873)
    * SFS: CPIO VFS skips empty directories in the root of archive (#1732)
    * Incorrect parsing FTP-string (#1605)
    * LZMA files detect (#1798)
    * FISH: broken filenames and timestamps on really poor busybox devices (#1450)
    * Minor mistakes and mistypes in man files
    * Various doxygen warnings


Version 4.7.0-pre4

- Core

    * Added feature of sort files by mouse click on column header
    * Added keybindings to change files sort type via shortcuts
      (PanelSelectSortOrder, PanelToggleSortOrderPrev, PanelToggleSortOrderNext,
      PanelReverseSort, PanelSortOrderByName, PanelSortOrderByExt,
      PanelSortOrderBySize, PanelSortOrderByMTime)
    * Now the letter of sort type and sort direction is always drawn in panel
      header (direction of sort is drown near the current sort column in long
      file list mode only)
    * Skin-files: added new parameters 'sort-sign-up' and 'sort-sign-down' in
      the section '[widget-common]' to draw sign of sort direction
    * Added option 'extensions_case' in filehighlight.ini file.
    * Menu engine was reimplemented: 1) now menu is build dynamically, 2)
      shortcut displayed in menu item is not a part of menu item text and it is
      synchronized with keybinding defined in mc.keymap file (#1563).

- VFS

    * Fixed view *.tar files with a colon in name
    * Allow 'exit' command on non-local filesystems
    * Added partial support of Microsoft CAB-archives
    * Added support of ico files
    * Added support of *.djvu files
    * Fix segfaults in various cases while browsing various VFSs
    * Fixed warnings when file copy inside archive.

- Editor

    * Added scrolled percentage in status bar (only in "simple statusbar" mode)
    * Fixed Misbehaving rectangular select in editor (wrong when selected from
      right to left and/or bottom to top)
    * Split editor menu 'Command' to 'Command' and 'Format'
    * Added option 'Check POSIX new line' into 'Save mode...' dialog, add notification
      before save when no newline at EOF (#46)
    * Added bindings ('EditShiftBlockLeft', 'EditShiftBlockRight') for shift block
    * Fixed incorrect drawing CJK (double width) character

- Viewer

    * Fixed tabs alignment (#1664)
    * Fixed view of next/prev file (#1750)

- Misc

    * Shared clipboard for the mc editor, form fields, panel and command prompt
    * Refactoring: now all filename constants are placed in src/fileloc.h file
    * Testing & development: added ability for change user config dir. For example,
      type make CFLAGS='-DMC_USERCONF_DIR=\".mc2\"'
    * Changed Alt+Backspace behavior in command line

- Fixes

    * Fixed broken building under uclibc <0.9.29
    * Autoindent regression
    * Viewer Hex editor regression
    * Incorrect indentation block when selecting with F3 + keys
    * Fixed ownership of config files
    * Configure.ac: fix broken check of option '--disable-vfs-undelfs'
    * Learn keys: fixed incorrect save of keys configuration
    * Fixed bug with 'The shell is already running a command'
    * Fixed 'B' artefact into OpenSuse console of root user


Version 4.7.0-pre3

- Core

    * removed own popt stuff (command line options parser). Now used glib parser
    * added feature for filenames highlighting in panels
    * Copy/Move overwrite query dialog is more friendly for long file names
    * at first run find file dialog now contain latest item from history
    * charset support enabled by default (--enable-charset option)
    * added support of skins
    * added support of key bindings

- VFS

    * fixed recognize of tar.xz archives
    * added recognize of lzma archives by extention

- Editor

    * 'Save as' dialog enhancement: select line break type: Windows/UNIX/Mac
      (CR LF/LF/CR)
    * syntax hightlighting updated: VerilogHDL, Shell script
    * Added syntax highlighting for *.repo files of yum
    * Added syntax highlighting of pacman's PKGBUILD and .install files

- Viewer

    * Fixed showing Windows/Mac (CR LF/CR) line terminator

- Misc

    * hotlist: support for environment variables ($HOME, ~username, etc.)
    * hotlist: support for completion in path input
    * all list widgets: support for fast navigation by number keys (i.e. 1 -
      first list item, 2 - second)

- Fixes

    * restored action of 'ctrl-t' keybinding (mark files&dirs). For selecting
      charset use 'alt-e'
    * segfault on incorrect color pair in [Color] section
    * incorrect position of panel codeset dialog
    * limit of 9 digits for of file size display
    * lines drawing in -a stickchars mode
    * segfault when you try to use non-anonymous FTP
    * Ctrl-O handling under GNU Screen in altscreen mode
    * support of CP866 (IBM866) locale
    * configure.ac: checking for minimal version of glib and exit if version less
      than 2.6
    * segfault by mouse wheel action in history list and menu
    * Fixed behvior with Meta+PgDn? in editor
    * Fixed behvior with cursor movement by Ctrl+arrows when cursor besides EOL
    * Fixed editor autocompleting
    * Fixed Copy/Move dialogs steal Kill Word shortcut
    * Fixed autoconf issue when configure with --with-gpm-mouse option


Version 4.7.0-pre2

- Core

    * cycle menu navigation
    * change behaviour of C-space, now it calculate size on "..", and for selected
      dirs if there is one.
    * new find file option: find only first hit in file (make search faster)
    * new find file option: Whole words - find whole words only
    * scalable TUI

- VFS

    * FTPFS: support of IPv6 protocol
    * extfs/iso9660 updated to support Joliet "UCS level 1"

- Editor

    * new search/replace flag added "In selection"
    * new hotkeys for bookmarks, now bookmark displayed in state line and editor
    * new cursor behavior. Option "Cursor beyond end of line" - allow moving cursor
      beyond the end of line
    * new syntax hightlights added: erlang, ebuild, named, strace, j
    * syntax hightlights updated: mail, vhdl, html

- Viewer

    * Reworked for improve perfomance
    * Implemented interruptable long-distance movements
    * splitted src/view.[ch] into many files in src/viewer/ subdir for more simple
      support of code
    * fixed build of format string in runtime (for better i18n)
    * add 'Whole words' option into the viewer 'Search' dialog

- Misc

    * new option mouse_close_dialog, if mouse_close_dialog=1 click on outside the
      dialog close them
    * new: SI-based size show
    * make shared history for find file, editor search/replace, viever

- Fixes

    * linking error with --enable-vfs-undelfs
    * external editor won't open if there are spaces in EDITOR variable
    * C-c kill mc if mc built with --without-subshell option is run with -d option
    * directory hotlist rendering
    * segfault on empty replace string
    * fixes for vfs/tarfs
    * removing bashizm from vfs/extfs/u7z
    * crash mc on create new file (Shift-F4) in external editor
    * File copy/move dialog: in replacement field now handled asterisks as search groups
    * VFS: Fixed SIGSERV(or heap corruption) on large filenames
    * Fixed broken backward search
    * Fixed uninitialised value for mouse event in in find.c#check_find_events() function
    * Fixed ctrl+z signal handling
    * Fixed incorrect showing prompt
    * Fixed incorrect vertical selection (if line state swiched on)
    * Fixed screen resize handle if mouse support is disabled
    * Restore correct current directory after switch from Info panel to List one
    * Fixed mouse support in 'konsole-256color' terminal
    * Fixed keycodes in 'xterm-256color' terminal
    * Fixed incorrect regexp search by content in 'file find' dialog
    * Fixed incorrect backwards search
    * Fixed incorrect detection of compressed patchfs
    * Fixed incorrect detecting codeset when <no translation> codeset selected


Version 4.7.0-pre1

- Core

    * native UTF-8 support
    * filenames charset selection support in panels
    * new 'Find File' dialog
    * new unified search/replace engine with search types: Plain, Wildcard, Regexp, Hex
    * extended learn keys
    * locale based codepage autodetection
    * initial support for doxygen generated docs
    * refresh of autoconf stuff
    * translation updates
    * x86_64 fixes

- Editor

    * various editor enchancements (vertical block marking/moving/copy/paste)
    * navigate across source code through ctags/etags TAGS file
    * new option 'Persistent selection'
    * Del/Backspace - delete selected block if 'Persistent selection' switched off
    * shift block right with TAB key by TAB size or TAB symbol, shift block left with COMPLETE key
      if 'Persistent selection' switched off
    * optional showing of line numbers
    * various syntax files updates
    * optional highlighting of the tabs and trailing spaces
    * add some hot-keys

- Misc

    * showing of the free space on current file system
    * showing of the size selected files in mini-status bar

- Fixes

    * editor's undo fixes
    * many fixes from other distributions are included
    * fish handling for symlinks is fixed
    * escaping of fancy names is fixed
    * segfault in fish permission checks is fixed
    * various mc.ext fixes
    * commandline completion fixes (mainly, escaping hadling)
    * small fixes in history handling (locale independent .mc/history entries)
    * code cleanups, various memleak/etc fixes (many thanks to valgrind)
